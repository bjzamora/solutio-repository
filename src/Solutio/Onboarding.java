package Solutio;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Onboarding {
	
	String filePath = "C:/Users/BJ Zamora/workspace/LearnAutomation.html";
	 ExtentReports  report;
	 ExtentTest  logger;
  @Test
  public void Onboard() throws InterruptedException
  {
	
	
	  
	   report = new ExtentReports(filePath, true);
	  
	    logger = report.startTest("Onboarding");
	
	  // set google chrome driver path
		System.setProperty("webdriver.chrome.driver", "C:/Users/BJ Zamora/Desktop/chromedriver_win32/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		   driver.manage().window().maximize();
		   logger.log(LogStatus.INFO, "Browser Started");
		   
		   //Navigate to URL
		   driver.get("https://sitsprinteight.192.168.1.240/onboarding/umbrella");
		   logger.log(LogStatus.INFO, "Navigate to onboarding/umbrella");		
		   Thread.sleep(5000); 
		   
		   //Verify Umbrella Registration Page
		   WebElement lblGetStarted = driver.findElement(By.className("catcher"));
		   String ActualGetStarted = lblGetStarted.getText();
		   System.out.println(ActualGetStarted);
		   
		   String a = "Get Started with Solutio";
		   Assert.assertEquals(ActualGetStarted, a);
		   
		
			   
		   //Create Date & Time Now
		   DateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
		   Date date = new Date();
		   String date1 = dateFormat.format(date);
		  
		   //Enter email
		   WebElement txtboxEmail = driver.findElement(By.xpath("//input[@type='email']"));
	
		   txtboxEmail.sendKeys(date1 + "umbrella@yopmail.com");
		   		Thread.sleep(5000); 
		   
		   //click submit button
		   driver.findElement(By.cssSelector("button.btn1")).click();
		
		   		Thread.sleep(5000);
		      
		   //Navigate to yopmail
			   driver.get("http://www.yopmail.com/en/");
			   	Thread.sleep(10000);
		   
		   //Enter email
		   WebElement txtboxYopEmail = driver.findElement(By.id("login"));
		   txtboxYopEmail.sendKeys(date1 + "umbrella");
		   
		   //Click Check Inbox button
		   WebElement btnYopCheckEmail = driver.findElement(By.cssSelector("input.sbut"));
		   btnYopCheckEmail.click();
		   		Thread.sleep(5000);
		    
		   //Click Email Message
		   driver.switchTo().frame(driver.findElement(By.id("ifinbox")));
		   driver.findElement(By.cssSelector("span.lmf")).click();
		   		Thread.sleep(1000); 
		   
		   //Click fast track registration
		   driver.switchTo().defaultContent();
		   driver.switchTo().frame("ifmail");
		   //driver.findElement(By.className("btn")).click(); 
		   driver.findElement(By.linkText("fast track registration")).click();
		   		Thread.sleep(2000); 
		   
		   // Switch to current page
		   ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		   driver.switchTo().window(tabs2.get(1));
		   
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle); }
		   
				Thread.sleep(3000);
		   
		   //Register 
		   new Select(driver.findElement(By.xpath("//form[@id='umbrellaOnboarding']/div[2]/div[2]/select"))).selectByVisibleText("Mr.");
		      
		   driver.findElement(By.xpath("//input[@type='text']")).sendKeys("fname");
		   
		   driver.findElement(By.xpath("(//input[@type='text'])[2]")).sendKeys("lname");
		   
		   driver.findElement(By.xpath("(//input[@type='text'])[3]")).sendKeys("0498328432");
		   
		   driver.findElement(By.xpath("(//input[@type='text'])[4]")).sendKeys("9085347985743");
		   
		   Thread.sleep(3000);
		   
		   //Create Date & Time Now for the Company email
		   DateFormat dateFormat5 = new SimpleDateFormat("MMddyyhhmmss");
		   Date date5 = new Date();
		   String date6 = dateFormat5.format(date5);
		   
		   driver.findElement(By.xpath("//input[@type='email']")).sendKeys(date6+"umbrella@yopmail.com");
		   
		   driver.findElement(By.xpath("(//input[@type='text'])[5]")).sendKeys("company");
		   
		   		Thread.sleep(2000);
		   
		   //Create Date & Time Now for the value of subdomain
		   DateFormat dateFormat10 = new SimpleDateFormat("MMddyyhhmmss");
		   Date subdomain = new Date();
		   String subdomain1 = dateFormat10.format(subdomain);
		   
		   driver.findElement(By.name("subdomain")).sendKeys(subdomain1);
		   
		   driver.findElement(By.xpath("//form[@id='umbrellaOnboarding']/div[10]/div[3]/div[2]/textarea")).sendKeys("description");
		   
		   driver.findElement(By.xpath("//form[@id='umbrellaOnboarding']/div[11]/label[2]")).click();
	
		   driver.findElement(By.xpath("//*[@id='umbrellaOnboarding']/div[12]/button")).click();
		   
		   //Write the subdomain in the hosts file
		   //NOTE. User must first configure and allow the privilege to be able to add subdomain in the hosts file 
		   //Else this code would not work
		   		Thread.sleep(11000); 
		
	  		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("C:/Windows/System32/drivers/etc/hosts", true))))
	  		{
	  		    out.println("192.168.1.240" + "		 "+subdomain1+".192.168.1.240");
	  		    //more code
	  		}catch (IOException e) {
	  		    //exception handling left as an exercise for the reader
	  		}
	  		
 
		   driver.close();
		 
   
		   
		   //Navigate back to the first tab (Yopmail)
		   driver.switchTo().window(tabs2.get(0));
		   
		
		   //Enter email
		   driver.findElement(By.id("login")).clear();
		   		Thread.sleep(3000);
		   driver.findElement(By.id("login")).sendKeys(date6+"umbrella@yopmail.com");
		   		Thread.sleep(3000);
		   //Click Check Inbox button
		   driver.findElement(By.xpath("//*[@id='lrefr']/span/span")).click();
		   		Thread.sleep(5000);
		    
		   //Click Email Message
		   driver.switchTo().frame(driver.findElement(By.id("ifinbox")));
		   driver.findElement(By.xpath("//*[@id='m1']/div/a/span[1]/span[2]")).click();
		   		Thread.sleep(1000); 
		   
		   //Click fast track registration
		   driver.switchTo().defaultContent();
		   driver.switchTo().frame("ifmail");
		   
		   //Get the Password inside the yopmail
			String Password = driver.findElement(By.xpath("//*[@id='mailmillieu']/div[2]/div/table/tbody/tr[2]/td")).getText();				
			   String PW = Password.substring(219, 231);
			
			//Click link to navigate to solutio  
			driver.findElement(By.xpath("//*[@id='mailmillieu']/div[2]/div/table/tbody/tr[2]/td/a[1]")).click();
				Thread.sleep(3000);
   
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle); }
			//Login to solutio											
		   driver.findElement(By.xpath("//*[@id='username']")).sendKeys(date1+"umbrella@yopmail.com");
		   
		   driver.findElement(By.xpath("//*[@id='pword']")).sendKeys(PW);
		   
		   driver.findElement(By.xpath("//*[@id='login-button']")).click();
		   
		   //Stores the Onboarded employee inside a text file
			try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("C:/tmp/User.txt", true))))
	  		{
				out.println("");
	  		    out.println("User Name: "+date1+"umbrella@yopmail.com");	  	
	  		    out.println("Password: "+PW);
	  		    out.println("Url: "+subdomain1+".192.168.1.240");
	  		}catch (IOException e) {
	  		    }
			   logger.log(LogStatus.PASS, "Onboard Succesfully");
				
		   
//			   report.endTest(logger);
//			   report.flush();
//			   
//			   driver.get("C:/Users/BJ Zamora/workspace/LearnAutomation.html");
  }
  @AfterMethod
  protected void afterEachTest(ITestResult result) {
      if (!result.isSuccess()) {
          logger.log(LogStatus.FAIL, result.getThrowable());
      }
      
      report.endTest(logger);        
      report.flush();
  }
  
  @AfterSuite
  protected void afterSuite() {
      report.close();
  }
  }
	       

  


