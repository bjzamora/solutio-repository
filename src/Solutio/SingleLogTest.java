package Solutio;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class SingleLogTest extends BaseExample {
    private final String filePath = "C:/tmp/Automation.html";
    
    @BeforeClass
    public void beforeClass() {
        extent = new ExtentReports(filePath, true);
        
        // optional - to store all results in a database
//        extent.startReporter(ReporterType.DB, "extent.db");
//        
//        extent.addSystemInfo("Host Name", "Anshoo");
    }
    
    @Test
    public void passTest() {
        test = extent.startTest("passTest");
        test.log(LogStatus.PASS, "Pass");
        
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
    
    @Test
    public void intentionalFailure() {
        test = extent.startTest("intentionalFailure");
        test.log(LogStatus.FAIL, "Fail");
        
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
   
    
}
